
function startCalculator()
{
    let num1 = prompt('Введите число 1'),
        num2 = prompt('Введите число 2'),
        action = prompt('Выберите действие (+, -, *, /)'),
        operations = ['+','-','/','*'],
        result = ''
        
    //optional
    setResultText('Тута будет результат!')

    //деление на 0
    if(+num2 === 0 && action === '/')
    {
        while(+num2 === 0 && action === '/')
        {
            alert('На 0 делить нельзя!')
            num1 = prompt('Введите число 1')
            num2 = prompt('Введите число 2')
            action = prompt('Выберите действие (+, -, *, /)')
        }
    }

    while(isNaN(num1) || isNaN(num2) || num1 === '' || num2 === '')
    {
        num1 = prompt('А теперь снова введите число 1',num1)
        num2 = prompt('А теперь снова введите число 2',num2)
    }

    while(operations.indexOf(action) === -1)
    {
        action = prompt('Так все же выберите действие из перечисленных (+, -, *, /)',action)
    }

    num1 = +num1
    num2 = +num2

    //вывод
    result = executor(num1,num2,action)
    console.log(result)

     //optional
     setResultText(result)
}

function executor(num1,num2,action)
{
    switch(action)
    {
        case '+':
            result = `${num1} ${action} ${num2} = ${num1 + num2}`
            break
        case '-':
            result = `${num1} ${action} ${num2} = ${num1 - num2}`
            break
        case '*':
            result = `${num1} ${action} ${num2} = ${num1 * num2}`
            break
        case '/':
            result = `${num1} ${action} ${num2} = ${num1 / num2}`
            break
        default:
            result = 'Ошибка при рассчетах'
            break
    }
    return result
}

//optional
function setResultText(text)
{
    let resultBlock = document.querySelector('.result')
    if(resultBlock)
    {
        resultBlock.innerHTML = text
    }
}